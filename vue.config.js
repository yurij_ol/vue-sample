module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/vue-sample'
    : '/',
  assetsDir: '',
  productionSourceMap: false,
  filenameHashing: true
}